import os
import traceback
import fnmatch
import psycopg2
import requests
from lxml import etree
import json
from copy import deepcopy
from pathlib import Path
from shutil import copyfile
from hmdk import HMDKUpdate
from migration import *
from getWorkspaces import *
#from dataset import *



proxies_fhh = getProxies()
empty_proxies = getEmptyProxies()

# Set Log Path and create empty array
file_path = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')
error = []

print("OAF Migration started")

make_end_folders()

hmdk = HMDKUpdate()
#downloadWorkspaces()
folderList = make_source_folderList()

# Diese Liste durchgehen
for folder in folderList:
    
    # in den Unterordner services wechseln
    servicesFolder = os.path.join(sourcePath, folder, "services")
    datasourcesFolder = os.path.join(sourcePath, folder, "datasources", "feature")
    if( os.path.exists(servicesFolder) and os.path.isdir(servicesFolder) ):
        
        # Alle wfs xml Dateien suchen
        onlyFiles = [f for f in os.listdir(servicesFolder) if os.path.isfile(os.path.join(servicesFolder, f)) and fnmatch.fnmatch(f, "wfs_*.xml")]
        for f in onlyFiles:
            if fnmatch.fnmatch(f, "*_ad.xml"):
                error.append(f + "|Hinweis,AD-geschützte Dienste werden aktuell nicht zu OAF migriert")
                #print(f + ",Hinweis,AD-geschützte Dienste werden aktuell nicht zu OAF migriert")
                continue
            if not fnmatch.fnmatch(f, "*_metadata.xml"):
                metadataFilename = os.path.splitext(f)[0] + "_metadata.xml"



                if os.path.isfile(os.path.join(servicesFolder, metadataFilename)):
                    newServiceFilename = f.replace("wfs_hh_", "")
                    newServiceFilename = newServiceFilename.replace("wfs_", "")

                    newMetadataFilename = metadataFilename.replace("wfs_hh_", "")
                    newMetadataFilename = newMetadataFilename.replace("wfs_", "")
                    
                    # Metadaten-Quelldatei parsen
                    try:
                        src_metadata_tree = etree.parse(os.path.join(servicesFolder, metadataFilename), parser)
                    except Exception as e:
                        message = 'Fehler: Problem mit XML-Datei'
                        #print(f + message + str(e))
                        error.append(f + "|" + message + ":" + str(e))
                        
                        continue

                    # Datenbank Connection erstellen
                    cursor = get_db_cursor()
                    service_name = os.path.splitext(f)[0]
                    dbResult = get_service_data_from_title(service_name, cursor)
                    try:
                        if dbResult is None:
                            #print(f  + "|Fehler: Keinen produktiven Datensatz in Dienstemanager Datenbank gefunden" + '|'  + service_name)
                            error.append(f + "|Fehler: Keinen produktiven Datensatz in Dienstemanager Datenbank gefunden" + '|'  + service_name)
                            continue
                        serviceID = dbResult[0]
                        serviceTitle = dbResult[1]
                        serviceURL = dbResult[2]
                        serviceVersion = dbResult[3]
                        serviceStatus = dbResult[4]
                        serviceOnlyIntranet = dbResult[5]
                        serviceOnlyInternet = dbResult[6]
                        serviceCheckedStatus = dbResult[7]
                        # if serviceCheckedStatus == "Fehler":
                        #     print(f + ",Warnung,Service Prüfstatus: " + serviceCheckedStatus)
                        #     continue

                        # Capabilities URL zusammenstellen und XML holen
                        getCapabilitiesURL = serviceURL + "?Service=WFS&Version=" + serviceVersion + "&Request=GetCapabilities"
                        try:
                            getCapabilitiesResponse = requests.get(getCapabilitiesURL,proxies=empty_proxies)
                        except:
                            error.append(f + "|Fehler: Problem Capabilities URL abzufragen, wahrscheinlich geschützter Dienst")
                            #print(f + " Fehler: Problem Capabilities URL abzufragen, wahrscheinlich geschützter Dienst")
                            continue
                        serviceCapabilities = etree.fromstring(getCapabilitiesResponse.content)

                        mult_MetadataSetcount = 0
                        # prüfen, ob mehrere Datensätze in einem Dienst vorhanden sind
                        if multiple_MetadataSetIds(src_metadata_tree):
                            mult_MetadataSetcount += 1
                            print('Multiple Metadataset IDs: ', newServiceFilename)
                            newServiceFilename = newServiceFilename.split(".")[0]
                            newMetadataFilename = newMetadataFilename.split("_metadata.")[0]
                            datasetMetadataURLs = get_DatasetMetadataURLs(serviceCapabilities)
                            for i, (datasetMetadataURL, names) in enumerate(datasetMetadataURLs.items()):
                                serviceFilename = newServiceFilename + "_" + str(i+1) + ".xml"
                                metadataFilename = newMetadataFilename + "_" + str(i+1) + "_metadata.xml"
                                
                                # Service-Quelldatei parsen
                                src_tree = etree.parse(os.path.join(servicesFolder, f), parser)
                                
                                # OAF Template Dateien parsen
                                serviceTemplateTree = etree.parse(oafServiceTemplatePath, parser)
                                metadataTemplateTree = etree.parse(oafMetadataTemplatePath, parser)

                                # Metaver.de durch HMDK URL austauschen
                                datasetMetadataURL = datasetMetadataURL.replace("http://www.metaver.de", "https://metaver.de")
                                hmdkURL = datasetMetadataURL.replace("https://metaver.de", "http://hmdk.fhhnet.stadt.hamburg.de")
                                datasetMetadataResponse = requests.get(hmdkURL, proxies = empty_proxies)
                                datasetMetadata = etree.fromstring(datasetMetadataResponse.content)
                                uuid = get_uuid(datasetMetadata)

                                if uuid is None:
                                    error.append(f  + "|Fehler: Keine Metadaten im HMDK XML?" + '|' + hmdkURL)
                                    #print(f + " Fehler: Keine Metadaten im HMDK XML?",'\n')
                                    continue

                                # OAF Metadata bearbeiten
                                datasetTitle = process_DatasetServiceIdentification(datasetMetadata, metadataTemplateTree, f)
                                process_ServiceProvider(serviceCapabilities, metadataTemplateTree)

                                # OAF Service bearbeiten
                                process_ServiceDatasetCreator(datasetMetadata, serviceTemplateTree)
                                process_ServiceMetadataURLs(serviceTemplateTree, uuid, serviceOnlyIntranet)
                                process_ServiceMetadata(datasetMetadata, serviceTemplateTree, f)
                                if process_ServiceFeatureStoreIdsWithFeatureNames(src_tree, serviceTemplateTree, datasourcesFolder, names, f, serviceID, serviceTitle) < 1:
                                    continue
                                
                                # OAF in HMDK eintragen
                                hmdk.write_url_ref(str(uuid), newServiceFilename.split(".")[0])
                                hmdk.write_obj_ref(str(uuid))
                                # OAF Service Template mit angepassten service Daten schreiben
                                save_XML_Files(serviceFilename,metadataTemplateTree, metadataFilename, serviceTemplateTree, serviceOnlyIntranet, serviceOnlyInternet)
                        else:
                             # Service-Quelldatei parsen
                            src_tree = etree.parse(os.path.join(servicesFolder, f), parser)
                            
                            # Datensatzmetadaten holen
                            datasetMetadataURL = get_DatasetMetadataURL(serviceCapabilities)
                            if datasetMetadataURL is None:
                                #print(getCapabilitiesURL)
                                error.append(f + "|Fehler: Keine MetadataURL in Capabilities gefunden" + '|' + getCapabilitiesURL)
                                #print(f + " Fehler: Keine MetadataURL in Capabilities gefunden", '\n')
                                continue

                            # OAF Template Dateien parsen
                            serviceTemplateTree = etree.parse(oafServiceTemplatePath, parser)
                            metadataTemplateTree = etree.parse(oafMetadataTemplatePath, parser)

                            # Metaver.de durch HMDK URL austauschen
                            
                            datasetMetadataURL = datasetMetadataURL.replace("http://www.metaver.de", "https://metaver.de")
                            hmdkURL = datasetMetadataURL.replace("https://metaver.de", "http://hmdk.fhhnet.stadt.hamburg.de")
                            datasetMetadataResponse = requests.get(hmdkURL, proxies = empty_proxies)                           
                            datasetMetadata = etree.fromstring(datasetMetadataResponse.content)
                            uuid = get_uuid(datasetMetadata)
                            
                            if uuid is None:
                                error.append(f  + "|Fehler: Keine Metadaten im HMDK XML?" + '|' + hmdkURL)
                                #print(f + " Fehler: Keine Metadaten im HMDK XML?",'\n')
                                continue

                            # OAF Metadata bearbeiten
                            datasetTitle = process_DatasetServiceIdentification(datasetMetadata, metadataTemplateTree, f)
                            process_ServiceProvider(serviceCapabilities, metadataTemplateTree)
                            
                            # OAF Service bearbeiten
                            process_ServiceDatasetCreator(datasetMetadata, serviceTemplateTree)
                            process_ServiceMetadataURLs(serviceTemplateTree, uuid, serviceOnlyIntranet)
                            process_ServiceMetadata(datasetMetadata, serviceTemplateTree, f)
                            if process_ServiceFeatureStoreIds(src_tree, datasourcesFolder, serviceTemplateTree, f, serviceID, serviceTitle) < 1:
                                continue

                            # OAF in HMDK eintragen
                            hmdk.write_url_ref(uuid, newServiceFilename.split(".")[0])
                            hmdk.write_obj_ref(uuid)

                            # OAF Service Template mit angepassten service Daten schreiben
                            save_XML_Files(newServiceFilename,metadataTemplateTree, newMetadataFilename, serviceTemplateTree, serviceOnlyIntranet, serviceOnlyInternet)
                    except Exception:
                        error.append(f + " and " + metadataFilename + f + "|Problems with source XML! Check Source files!")
                        #print(f + " and " + metadataFilename + f + '\n' + ": Problems with source XML! Check Source files!")
                        
                        traceback.print_exc()
                    close_db_connection(cursor)
                else:
                    error.append(f + "|Fehler: Keine passende Metadaten XML Datei " + metadataFilename + " gefunden")
                    #print(f + '\n' + "Fehler: Keine passende Metadaten XML Datei " + metadataFilename + " gefunden")

#write errors to file
with open(os.path.join(file_path,'error_migration.json'), 'w', encoding='utf-8') as f:
    json.dump(error,f, ensure_ascii=False, indent=4)    

print("OAF Migration finished")
