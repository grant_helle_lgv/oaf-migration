import os
import requests
from migration import *
import zipfile

proxies = getProxies()
empty_proxies = {'http': '','https': '',} 

def getEmptyProxies():
    return empty_proxies

end_path = getPath()

url = 'http://wfalgpa002.dpaorinp.de/fachdaten_a_b/config/download'

def getWorkspaces():
    list_workspaces = 'http://wfalgpa002.dpaorinp.de/fachdaten_a_b/config/listworkspaces'
    response = requests.get(list_workspaces,proxies=empty_proxies)
    workspaces = []
    for line in response.text.splitlines():
        workspaces.append(line)
    return workspaces

def saveFile(path, response):
    with open(path, "wb") as f:
        f.write(response.content)   
    print('File saved to: ' + path)

def extract(source_path, end_path, workspace_name):
    with zipfile.ZipFile(source_path, 'r') as zip_ref:
        zip_ref.extractall(os.path.join(end_path, workspace_name))
        print(end_path + workspace_name + ' extracted')
    return

def downloadWorkspaces():
    url_list = []
    for workspace_name in getWorkspaces():
        path = os.path.join(end_path, 'zips', workspace_name + '.zip')
        url = f"http://wfalgpa002.dpaorinp.de/{workspace_name}/config/download"
        response = requests.get(url,proxies=empty_proxies)
        saveFile(path, response)
        try:
            extract(path, getWorkspacesPath(), workspace_name)
        except:
            continue
    return



