
from migration import *
from dataset import Dataset, hmdk_cursor, getObjRefs, getURLRefs, deleteOrphanObj, deleteOrphanURL
import pprint
pp = pprint.PrettyPrinter()


#Find workspaces to be converted
folder_list = make_source_folderList()
sourcePath = getWorkspacesPath()

wfs_service_list = []
wfs_metadata_list = []
wfst_list = []
wms_list = []
ad_list = []
other_services_list = []

#Parse through workspaces folders
service_name_list = get_Service_Names_List(folder_list)
service_files_list = get_Service_files_List(folder_list)

#GET WFS files
count =  0
collection_xml_list = []
for service_file in service_files_list:
    if get_WFS(service_file):
        collection_xml_list.append(service_file)
    else:
        continue

#get service names
for service_file in service_name_list:
    if get_AD(service_file):
        ad_list.append(service_file)

    if get_WFS_Meta(service_file):
        wfs_metadata_list.append(service_file)

    elif get_WMS(service_file):
        wms_list.append(service_file)
        
    elif get_WFST(service_file):
        wfst_list.append(service_file)
        
    elif get_WFS(service_file):
        wfs_service_list.append(service_file)

    else:
        other_services_list.append(service_file)

gid = 0
dataset_list = []
while gid < len(collection_xml_list):
    wfs_service = collection_xml_list[gid]
    #Create Dataset Object and instantiate
    dataset = Dataset(gid, wfs_service)
    dataset_list.append(dataset)
    gid += 1

uuid_list = []
no_workspace_count = 0
total_workspaces = 0

for gid in range(len(dataset_list)):
    #Print all datasets and attributes
    #pp.pprint(dataset_list[gid].__dict__)
    #print('\n')
    #Find Orphans and missing links
    for i in dataset_list[gid].uuids:
        uuid_list.append(i)

obj_ref_list = getObjRefs(hmdk_cursor)
url_ref_list = getURLRefs(hmdk_cursor)
#print('No obj_ref in db', set(uuid_list).difference(obj_ref_list))
#print(url_ref_list)

#Compare list of HDMK Object References + URL references with list of all active uuids

#orphanObjRefs = set(obj_ref_list).difference(uuid_list)
missingObjRefs = set(uuid_list).difference(obj_ref_list)
#print('Orphans Obj Refs: ', orphanObjRefs)
print('Missing Obj Refs: ', missingObjRefs)
#deleteOrphanObj(hmdk_cursor, orphanObjRefs)

orphanURLRefs = set(url_ref_list).difference(uuid_list)
missingURLRefs = set(uuid_list).difference(url_ref_list)
#print('Orphan URL Refs: ', orphanURLRefs)
print('Missing URL Refs: ', missingURLRefs)
#deleteOrphanURL(hmdk_cursor, orphanURLRefs)


'''
    #Count and print workspaces not moved and not working
    no_workspace_count
    if dataset_list[gid].reqStatus == 'Failed':
        no_workspace_count+=1
    else: 
        total_workspaces +=1

print('Missing workspaces', no_workspace_count)   
print('Working workspaces', total_workspaces)  
'''
