
# public.t01_object = HDMK Seiten
# public.t017_url_ref = weitere Verweise
# public.object_reference = Datensatzkopplung

# Verbindung:
# t01_object.id <-> t017_url_ref.obj_id



import os
import psycopg2
from migration import *

content = 'OGC API - Features (OAF) Landing Page'

class HMDKUpdate:
    """
    Diese Klasse stellt Funktionen zum Einfügen von Verweisen zu OGC API - Features im HMDK bereit
    Jira: https://www.jira.geoportal-hamburg.de/browse/UDPI-186
    """

    def __init__(self):
        self.id_assigned = False

    def get_hmdk_db_cursor(self):
        conn = psycopg2.connect(
            database = "igc_hh",
            #QS host unter: qs-anwendungen.dpaorinp.de
            host = "gv-srv-w00114",
            port = 5432,
            user = "postgres",
            password = "postgres"
        )
        return conn.cursor()

    def close_db_connection(self, cursor):
        cursor.close()
        cursor.connection.close()

    def get_hibernate_id(self, cur):
        cur.execute("UPDATE public.hibernate_unique_key SET next_hi = next_hi + 1 RETURNING next_hi")
        new_id = cur.fetchone()[0]
        cur.connection.commit()
        return new_id * 32767

    def get_new_id(self):
        self.id = self.id + 1
        return self.id

    def url_ref_exists(self, obj_id, url_link, cur):
        cur.execute("SELECT * FROM t017_url_ref WHERE obj_id = %s AND url_link = %s OR obj_id is null AND url_link = %s", (obj_id, url_link, url_link))
        return cur.fetchone() is not None

    def obj_ref_exists(self, obj_to_uuid, oaf_obj, cur):
        cur.execute("SELECT * FROM object_reference WHERE obj_to_uuid = %s and obj_from_id = %s", (obj_to_uuid, oaf_obj))
        return cur.fetchone() is not None

    def get_object_id_from_uuid(self, uuid, cur):
        cur.execute("SELECT id FROM t01_object WHERE obj_uuid = %s", [uuid])
        try: 
            return cur.fetchone()[0]
        except TypeError:
            print('No uuid for ' + uuid)
            return None

    
    def get_oaf_obj(self, cur):
        cur.execute("select * from t01_object where obj_name like '%OAF%'")
        return cur.fetchone()[0]

    def get_next_line_number(self, obj_id, cur):
        cur.execute("SELECT MAX(line)+1 as nextline FROM t017_url_ref WHERE obj_id = %s", [obj_id])
        return cur.fetchone()[0]

    def get_next_line_obj_ref(self, obj_to_uuid, cur):
        cur.execute("SELECT MAX(line)+1 as nextline FROM object_reference WHERE obj_to_uuid = %s", [obj_to_uuid])
        try: 
            cur.fetchone()[0]
            return cur.fetchone()[0]
        except TypeError:
            return 0

    def add_url_reference(self, data, cur):
        cur.execute("""INSERT INTO public.t017_url_ref (id, version, obj_id, line, special_ref, special_name, content, descr, url_type, datatype_key, datatype_value, url_link) 
                    VALUES (%(id)s, %(version)s, %(obj_id)s, %(line)s, %(special_ref)s, %(special_name)s, %(content)s, %(descr)s, %(url_type)s, %(datatype_key)s, %(datatype_value)s, %(url_link)s);""", data)
        cur.connection.commit()
        close_db_connection(cur)

    def add_obj_ref(self, data, cur):
        cur.execute("""INSERT INTO public.object_reference (id, version, obj_from_id, obj_to_uuid, line, special_ref, special_name, descr) 
                    VALUES (%(id)s, %(version)s, %(obj_from_id)s, %(obj_to_uuid)s, %(line)s, %(special_ref)s, %(special_name)s, %(descr)s);""", data)
        cur.connection.commit()
        close_db_connection(cur)

    def write_url_ref(self, uuid, datasetName):
        #Base data dictions to be writen to db
        data = {
            "id": 0, 
            "version": 0, 
            "obj_id": 0, 
            "line": 0, 
            "special_ref": 9999, 
            "special_name": "unspezifischer Verweis", 
            "content": content, 
            "descr": None,
            "url_type": 1, 
            "datatype_key": None,
            "datatype_value": None, 
            "url_link": "https://geodienste.hamburg.de/HH_OAF/datasets/" + datasetName}
        
        hmdk_cursor = self.get_hmdk_db_cursor()

        #fill with correct object ID using uuid
        if uuid is None:
            print('No uuid!')
            return
        else:
            data["obj_id"] = self.get_object_id_from_uuid(uuid, hmdk_cursor)
        
        #print("url link: ",data["url_link"])
        
        #Checks URL if it already exists
        if not self.url_ref_exists(data["obj_id"], data["url_link"], hmdk_cursor):
            #If not first assign key
            #Ensure it is correct
            if not self.id_assigned:
                self.id = self.get_hibernate_id(hmdk_cursor)
                self.id_assigned = True
                data["id"] = self.id

            else:
                data["id"] = self.get_new_id()

            #And add line to table public.reference
            data["line"] = self.get_next_line_number(data["obj_id"], hmdk_cursor)
            self.add_url_reference(data, hmdk_cursor)
        self.close_db_connection(hmdk_cursor)

    #Write into table object_reference to join dataset with OAF Mother Object        
    def write_obj_ref(self, uuid):
            #Base data dictions to be writen to db
            hmdk_cursor = self.get_hmdk_db_cursor()
            oaf_obj = self.get_oaf_obj(hmdk_cursor) 

            data = {
                "id": 0, 
                "version": 0, 
                #obj_id of OAF Mother Object
                "obj_from_id": oaf_obj, 
                "obj_to_uuid": '0',
                "line": 0, 
                "special_ref": 3600, 
                "special_name": "Gekoppelte Daten", 
                "descr": None,
                }
            
            #fill with correct object ID using uuid
            data["obj_to_uuid"] = uuid
            #Checks URL if it already exists
            if not self.obj_ref_exists(data["obj_to_uuid"],data["obj_from_id"], hmdk_cursor):
                #If not first assign key
                #Ensure it is correct
                if not self.id_assigned:
                    self.id = self.get_hibernate_id(hmdk_cursor)
                    self.id_assigned = True
                    data["id"] = self.id 

                else:
                    data["id"] = self.get_new_id()
                    #print('New ID = ' + data["id"])

                #And add line to table public.reference
                data["line"] = self.get_next_line_obj_ref(data["obj_to_uuid"], hmdk_cursor)
                self.add_obj_ref(data, hmdk_cursor)
            #print('Written to HMDK: ', uuid)
            self.close_db_connection(hmdk_cursor)    
