import json
import os
import requests
import csv
from migration import *
from hmdk import content, close_db_connection
from lxml import etree

def get_hmdk_db_cursor():
    conn = psycopg2.connect(
        database = "igc_hh",
        #QS host unter: qs-anwendungen.dpaorinp.de
        host = "gv-srv-w00114",
        port = 5432,
        user = "postgres",
        password = "postgres"
    )
    return conn.cursor()

hmdk_cursor = get_hmdk_db_cursor()



def _makeRequest(url, proxies = {'http': '', 'https':''}):
    req = requests.get(url, proxies=proxies)
    return req

#Define dataset class including all relevant infomation and data
class Dataset:
    def __init__(self, gid, file_name):
        self.gid = gid
        self.file_name = file_name
        self.getMetadataFileName()
        self.wfs_service_name = os.path.basename(self.file_name).replace(".xml", "")
        self.getCapabilities()
        self.getCollectionName()
        self.url = f'http://localhost:8081/deegree-services-oaf/datasets/{self.collection_name}/collections'
        self.reqURL(self.url)  
        self.getOAFConfig()  
        self.reqCapabilities()
        self.num_datasets = len(self.md_urls)
        self.getUUIDs()
        self.checkObjRef(hmdk_cursor)
        self.checkURLRef(hmdk_cursor)

    def getCollectionName(self):
        collection_name = self.wfs_service_name.replace("wfs_hh_", "")
        self.collection_name = collection_name.replace(".xml", "")
        return self.collection_name

    def getMetadataFileName(self):
        self.metadata_file_name = self.file_name.replace(".xml", "_metadata.xml")
        return self.metadata_file_name


    def reqURL(self, url):
        request = _makeRequest(url)
        reqStatus= request.status_code
        if reqStatus != 200:
            url = url.replace("/collections", "_1/collections")
            request = _makeRequest(url)
            reqStatus= request.status_code

            if reqStatus != 200:
                self.reqStatus = 'Failed'
                return reqStatus
                
            else:
                self.reqStatus = 'Success'
                return reqStatus

        else:
            self.reqStatus = 'Success'
            return reqStatus

    def getOAFConfig(self):
        self.oaf_config = os.path.join(EndPath, 'ogcapi', self.collection_name + '.xml')
        oaf_config_multi = os.path.join(EndPath, 'ogcapi', self.collection_name + '_1' + '.xml')
        #print('working', self.oaf_config, end = "\r")
        if os.path.isfile(self.oaf_config):
            return self.oaf_config
        
        elif os.path.isfile(oaf_config_multi): 
            self.oaf_config = oaf_config_multi
            return self.oaf_config

        else:
            
            self.oaf_config = 'Error'
            return self.oaf_config

    def getCapabilities(self):
        cur = get_db_cursor()
        dbResult = get_service_data_from_title(self.wfs_service_name, cur)
        
        try:
            if dbResult is None:
                self.getCapabilitiesURL = 'nicht gefunden'
                return self.getCapabilitiesURL

            service_url= dbResult[2]
            serviceVersion = dbResult[3]
            self.getCapabilitiesURL = service_url + "?Service=WFS&Version=" + serviceVersion + "&Request=GetCapabilities"
            return self.getCapabilitiesURL

        except:
            self.getCapabilitiesURL = 'skipped'
            return self.getCapabilitiesURL


    def reqCapabilities(self):
        try:
            request = _makeRequest(self.getCapabilitiesURL)
            serviceCapabilities = etree.fromstring(request.content)

            dictionary = get_DatasetMetadataURLs(serviceCapabilities)
            #print(len(dictionary))
            #print(dictionary.keys(),'\n')
            self.md_urls = list(dictionary.keys())
            #self.num_datasets = len(dictionary)
            return self.md_urls
        except:
            self.md_urls =  []
            return self.md_urls

    def getUUIDs(self):
        self.uuids = []
        for i in range(len(self.md_urls)):
            text = self.md_urls[i]
            text = text.split('&id=')[1]
            text = text.split('&outputSchema=')[0]
            self.uuids.append(text)
        return self.uuids
        
    def checkObjRef(self, cur):
        '''Make sure only 1 connection is made'''
        #Set false is no uuids present
        if not self.uuids:
            self.hmdk_kopplung = False
        try:
            for i in self.uuids:
                cur.execute("SELECT * FROM public.object_reference where obj_to_uuid = %s and obj_from_id = '1412661249'", [i])
                rows = cur.fetchall()
                count = 0
                for row in rows:
                    count += 1
                
                    if count == 1:
                        self.hmdk_kopplung = True
                    
                    elif count == 0:
                        print('No HDMK Entries!',uuid)
                        self.hmdk_kopplung = False     

                    else:
                        print('Multiple HDMK Entries!',uuid)
                        self.hmdk_kopplung = False
                
        except:
            self.hmdk_kopplung = False
        
    def checkURLRef(self, cur):
        if not self.uuids:
            self.hmdk_link = False
        try:
            for i in self.uuids:
                cur.execute("SELECT u.obj_id, o.obj_uuid, o.obj_name, u.content from t017_url_ref u, t01_object o where u.content = %s and u.obj_id = o.id and obj_uuid = %s", [content, i])
                rows = cur.fetchall()
                count = 0
                for row in rows:
                    count += 1
                
                    if count == 1:
                        self.hmdk_link = True
                    
                    elif count == 0:
                        print('No HDMK Entries!',uuid)
                        self.hmdk_link = False     

                    else:
                        print('Multiple HDMK Entries!',uuid)
                        self.hmdk_link = False
        except:
            self.hmdk_link = False
    
def getObjRefs(cur):
    obj_ref_list = []
    cur.execute("SELECT obj_to_uuid FROM public.object_reference where obj_from_id = '1412661249'")
    rows = cur.fetchall()
    for row in rows:
        obj_ref_list.append(row[0])
    return obj_ref_list

def getURLRefs(cur):
    url_ref_list = []
    cur.execute("SELECT o.obj_uuid from t017_url_ref u, t01_object o where u.content = %s and u.obj_id = o.id", [content])
    rows = cur.fetchall()
    for row in rows:
        url_ref_list.append(row[0])
    return url_ref_list

def deleteOrphanObj(cur, orphan_list):
    for i in orphan_list:
        
        try:
            cur.execute("delete FROM public.object_reference where obj_to_uuid = %s", [i])
            print('Deleted: ', i)
        except:
            print('Could not be deleted')
    cur.connection.commit()
    close_db_connection(cur)

def deleteOrphanURL(cur, orphan_list):
    for i in orphan_list:
        
        try:
            cur.execute("delete from t017_url_ref u using t01_object o where u.content = %s and u.obj_id = o.id and o.obj_uuid = %s", [content, i])
            print('Deleted: ', i)
        except:
            print('Could not be deleted', i)
    cur.connection.commit()
    close_db_connection(cur)