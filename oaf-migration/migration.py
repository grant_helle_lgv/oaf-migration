import os
import traceback
import fnmatch
import psycopg2
import requests
from lxml import etree
import json
from copy import deepcopy
from pathlib import Path
from shutil import copyfile

parser = etree.XMLParser(remove_blank_text=True)

proxies_fhh = {
                'http': 'http://wall.lit.hamburg.de:80',
                'https': 'http://wall.lit.hamburg.de:80',    
                }

base_url = 'https://geodienste.hamburg.de/HH_OAF/datasets/'

# Pfade festlegen
path = "C:\\Users\\HelleGr\\Desktop\\oaf-migra-export"
EndPath = "C:\\_Lokale_Daten_ungesichert\\ogc-api\\deegree\\hhlgvoaf-workspace\\workspace\\ogcapi-workspace"
sourcePath = os.path.join(path, "workspaces")

intranetServicesEndFolder = os.path.join(EndPath,"ogcapi")
intranetMetadataEndFolder = os.path.join(EndPath, "services")
internetServicesEndFolder = os.path.join(EndPath,"internet","ogcapi")
internetMetadataEndFolder = os.path.join(EndPath,"internet","services")
dsEndFolder = os.path.join(EndPath, "datasources", "generated")

oafServiceTemplatePath = os.path.join(path, "oaf_service_template.xml")
oafMetadataTemplatePath = os.path.join(path, "oaf_metadata_template.xml")

namespaces = {
    "wfs": "http://www.opengis.net/wfs",
    "wfs2": "http://www.opengis.net/wfs/2.0",
    "ows": "http://www.opengis.net/ows",
    "ows1.1": "http://www.opengis.net/ows/1.1",
    "dee_wfs": "http://www.deegree.org/services/wfs",
    "dee_sql": "http://www.deegree.org/datasource/feature/sql",
    "dee_metadata": "http://www.deegree.org/services/metadata",
    "oaf": "http://www.deegree.org/ogcapi/features",
    "xlink": "http://www.w3.org/1999/xlink",
    "gmd": "http://www.isotc211.org/2005/gmd",
    "gco": "http://www.isotc211.org/2005/gco"
}

# path = "C:\\Entwicklung\\udp-intern\\Diverses\\oaf_service_migration\\test"
# sourcePath = os.path.join(path, "workspaces_test")

def getPath():
    return path

def getWorkspacesPath():
    return sourcePath

def getProxies():
    return proxies_fhh
    
# Make folders for final OAF config files if they dont already exist
def make_end_folders():
    Path(intranetServicesEndFolder).mkdir(parents=True, exist_ok=True)
    Path(intranetMetadataEndFolder).mkdir(parents=True, exist_ok=True)
    Path(internetServicesEndFolder).mkdir(parents=True, exist_ok=True)
    Path(internetMetadataEndFolder).mkdir(parents=True, exist_ok=True)
    Path(dsEndFolder).mkdir(parents=True, exist_ok=True)
    print(dsEndFolder)


# Alle Ordner im SourcePath in einer Liste speichern
def make_source_folderList():
    folderList = []
    for f in os.scandir(sourcePath):
        if f.is_dir():
            folderList.append(f.name)
    return folderList

def get_Services_Folders_List(folder_list):
    services_folders_list = []
    for folder in folder_list:
        servicesFolder = os.path.join(sourcePath, folder, "services")
        services_folders_list.append(servicesFolder)
    return services_folders_list


def get_Service_files_List(folder_list):
    service_files_list = []
    services_folders_list = get_Services_Folders_List(folder_list)
    for folder in range(len(services_folders_list)):
        for wfs_file in os.listdir(services_folders_list[folder]):
            folder_path = services_folders_list[folder]
            service_files_list.append(os.path.join(folder_path, wfs_file))
    return service_files_list

def get_Service_Names_List(folder_list):
    service_name_list = []
    services_folders_list = get_Services_Folders_List(folder_list)
    for folder in range(len(services_folders_list)):
        for wfs_file in os.listdir(services_folders_list[folder]):
            service_name_list.append(wfs_file)
    return service_name_list

'''
def get_Service_Name(service_file):
    service_file_name = os.path.basename(service_file)
    print(service_file_name)
'''

def get_AD(service_file):
    if fnmatch.fnmatch(service_file, "*_ad*.xml"):
        return True

def get_WMS(service_file):
    if fnmatch.fnmatch(service_file, "*wms_*.xml"):
        return True

def get_WFST(service_file):
    if fnmatch.fnmatch(service_file, "*wfst_*.xml") or fnmatch.fnmatch(service_file, "*wfs_t_*.xml"):
        return True


def get_WFS_Meta(service_file):
    if fnmatch.fnmatch(service_file, "*_metadata.xml") and fnmatch.fnmatch(service_file, "*wfs_*.xml"):
        return True

def get_WFS(service_file):
    if fnmatch.fnmatch(service_file, "*wfs_*.xml") and not get_WFS_Meta(service_file) and not get_WFST(service_file) and not get_AD(service_file):
        return True

def get_db_cursor():
    """Connect to database an return cursor to start requests"""
    conn = psycopg2.connect(
      database = "dienstemanager",
      host = "prod-anwendungen.dpaorinp.de",
      port = 5433,
      user = "dienstemanager",
      password = "VabitNRnZxlKvdWZpdQC"
    )
    return conn.cursor()

def close_db_connection(cursor):
    """Closes database connection"""
    cursor.close()
    cursor.connection.close()

def get_service_data_from_title(service_name, cur):
    """Start request to dienstemanager database to return service information with given service name"""
    cur.execute("SELECT service_id, title, url_int, version, status, only_intranet, only_internet, service_checked_status FROM service_list WHERE status = 'Produktiv' AND service_name = %s", [service_name])
    return cur.fetchone()

def get_DatasetMetadataURLs(capabilities):
    """Searches for the MetadataURL tag in given capabilities XML and returns all found URLs as a dict(url: name)"""
    urls = dict()
    featureTypes = capabilities.findall(".//wfs:FeatureType", namespaces)
    for featureType in featureTypes:
        title = featureType.find(".//wfs:Title", namespaces)
        metadataURL = featureType.find(".//wfs:MetadataURL", namespaces)
        if title is not None and metadataURL is not None:
            if title.text is not None and metadataURL.text is not None:
                name = title.text.split(":")[1]
                if metadataURL.text in urls:
                    urls[metadataURL.text].append(name)
                else:
                    urls[metadataURL.text] = [name]
    return urls

def get_DatasetMetadataURL(capabilities):
    """Searches for the MetadataURL tag in given capabilities XML and returns the URL"""
    metadataURL = capabilities.find(".//wfs:MetadataURL", namespaces)
    
    if metadataURL is not None:
        if metadataURL.text is not None:
            return metadataURL.text
    else:
        metadataURL = capabilities.find(".//wfs2:MetadataURL", namespaces)
        if metadataURL is not None:
            return metadataURL.attrib["{http://www.w3.org/1999/xlink}href"]
    return None

def replaceMetaverURLWithHMDK(oafServiceTemplate):
    metadataURLs = oafServiceTemplate.findall(".//oaf:MetadataURL", namespaces)
    for metadataURL in metadataURLs:
        if metadataURL.text is not None:
            metadataURL.text = metadataURL.text.replace("https://metaver.de", "http://hmdk.fhhnet.stadt.hamburg.de")

def process_ServiceMetadataURLs(oafServiceTemplate, uuid, serviceOnlyIntranet):
    """Adds the service MetadataURL to the OAF template"""
    serviceMetadataURL = "https://metaver.de/csw?Service=CSW&Request=GetRecordById&Version=2.0.2&id=" + uuid + "&outputSchema=http://www.isotc211.org/2005/gmd&elementSetName=full"
    metadataURLApplication = oafServiceTemplate.find('.//*[.="MetadataURLApplication"]')
    if metadataURLApplication.text:
        metadataURLApplication.text = metadataURLApplication.text.replace("MetadataURLApplication", serviceMetadataURL)
    metadataURLText = oafServiceTemplate.find('.//*[.="MetadataURLText"]')
    if metadataURLText.text:
        metadataURLText.text = metadataURLText.text.replace("MetadataURLText", "https://metaver.de/trefferanzeige?docuuid=" + uuid)

# TODO clean up
def copyDatasourceFile(dsPathArray,dsEndFolder, dsFilePath):
    if len(dsPathArray) == 1:
        copyfile(dsFilePath, os.path.join(dsEndFolder, dsPathArray[0] + ".xml"))
    if len(dsPathArray) == 2:
        Path(dsEndFolder, dsPathArray[0]).mkdir(parents=True, exist_ok=True)    
        copyfile(dsFilePath, os.path.join(dsEndFolder, dsPathArray[0], dsPathArray[1] + ".xml"))
    if len(dsPathArray) == 3:
        Path(dsEndFolder, dsPathArray[0], dsPathArray[1]).mkdir(parents=True, exist_ok=True)    
        copyfile(dsFilePath, os.path.join(dsEndFolder, dsPathArray[0], dsPathArray[1], dsPathArray[2] + ".xml"))
    if len(dsPathArray) > 3:
        print("Fehler,Datasource-Ordner hat mehr als 3 Unterordner:", dsFilePath)

def process_ServiceFeatureStoreIds(tree, datasourcesFolder, oafServiceTemplate, f, serviceID, serviceTitle):
    """Adds the service FeatureStoreIds to the OAF template and searches for corresponding datasource files to copy them to the predefined datasource folder"""
    numOfFeatureStores = 0
    featureStoreIds = tree.findall(".//dee_wfs:FeatureStoreId", namespaces)
    if len(featureStoreIds) == 0:
        print(f + ",Fehler,Keine FeatureStoreIds zugeordnet!")
        return numOfFeatureStores
    prevDatasourcePath = ""
    for featureStoreId in featureStoreIds:
        datasourcePath = os.path.join(datasourcesFolder, featureStoreId.text + ".xml")
        datasource = etree.parse(datasourcePath, parser)
        geometries = datasource.findall(".//dee_sql:Geometry", namespaces)
        if len(geometries) > 1:
            if prevDatasourcePath != datasourcePath:
                prevDatasourcePath = datasourcePath
                #print(featureStoreId.text + ".xml" + ",Fehler,Datasource enthält mehr als eine Geometrie!")
            continue
        featureStoreId.tag = etree.QName(featureStoreId).localname
        oafServiceTemplate.getroot().insert(0, featureStoreId)
        numOfFeatureStores += 1

        copyDatasourceFile(featureStoreId.text.split("/"), dsEndFolder,os.path.join(datasourcesFolder, featureStoreId.text + ".xml"))
    return numOfFeatureStores

def process_ServiceFeatureStoreIdsWithFeatureNames(tree, oafServiceTemplate, datasourcesFolder, names, f, serviceID, serviceTitle):
    """Adds the service FeatureStoreIds to the OAF template if it matches the names[] and searches for corresponding datasource files to copy them to the predefined datasource folder"""
    numOfFeatureStores = 0
    featureStoreIds = tree.findall(".//dee_wfs:FeatureStoreId", namespaces)
    if len(featureStoreIds) == 0:
        print(f + ",Fehler,Keine FeatureStoreIds zugeordnet!")
        return numOfFeatureStores
    prevDatasourcePath = ""
    for featureStoreId in featureStoreIds:
        datasourcePath = os.path.join(datasourcesFolder, featureStoreId.text + ".xml")
        datasource = etree.parse(datasourcePath, parser)
        geometries = datasource.findall(".//dee_sql:Geometry", namespaces)
        if len(geometries) > 1:
            if prevDatasourcePath != datasourcePath:
                prevDatasourcePath = datasourcePath
                #print(featureStoreId.text + ".xml" + ",Fehler,Datasource enthält mehr als eine Geometrie!")
            continue
        featureTypeMapping = datasource.find(".//dee_sql:FeatureTypeMapping", namespaces)
        if featureTypeMapping.get("name") in names:
            featureStoreId.tag = etree.QName(featureStoreId).localname
            oafServiceTemplate.getroot().insert(0, featureStoreId)
            numOfFeatureStores += 1
        else:
            name = featureTypeMapping.get("name").split(":")[-1]
            if name in names:
                featureStoreId.tag = etree.QName(featureStoreId).localname
                oafServiceTemplate.getroot().insert(0, featureStoreId)
                numOfFeatureStores += 1
        
        copyDatasourceFile(featureStoreId.text.split("/"),dsEndFolder, os.path.join(datasourcesFolder, featureStoreId.text + ".xml"))
    return numOfFeatureStores

def multiple_MetadataSetIds(tree):
    """Returns true if the metadata holds more than one MetadataSetId"""
    ids = []
    allIds = tree.findall(".//dee_metadata:MetadataSetId", namespaces)
    for metadataSetId in allIds:
        if metadataSetId.text is not None:
            ids.append(metadataSetId.text)
    if len(ids) > 0:
        return len(set(ids)) != 1
    else:
        return False

def is_json(text):
    """Returns true if given string is JSON"""
    try:
        text = json.loads(text)
    except ValueError:
        return False
    return True

def findAndReplaceTagString(xml, searchString, replaceString):
    """Helper function to find a specific xml tag and replace the text inside with given string"""
    elem = xml.find('.//*[.="'+searchString+'"]')
    if elem is not None:
        if elem.text is not None:
            elem.text = elem.text.replace(searchString, replaceString)
 
def process_ServiceMetadata(datasetMetadataXML, oafServiceTemplate, f):
    """Adds multiple tags and strings from the dataset metadata to the OAF template"""
    resourceConstraints = datasetMetadataXML.findall(".//gmd:resourceConstraints", namespaces)
    characterStrings = resourceConstraints[0].findall(".//gco:CharacterString", namespaces)

    jsonStringFound = False
    for constraintString in characterStrings:
        if is_json(constraintString.text) and not jsonStringFound:
            jsonStringFound = True
            metadataLegalConstraints = json.loads(constraintString.text)
            findAndReplaceTagString(oafServiceTemplate, "ProviderLicenseName", metadataLegalConstraints["name"])
            findAndReplaceTagString(oafServiceTemplate, "ProviderLicenseWebsite", metadataLegalConstraints["url"])
            findAndReplaceTagString(oafServiceTemplate, "DatasetLicenseName", metadataLegalConstraints["name"])
            findAndReplaceTagString(oafServiceTemplate, "DatasetLicenseWebsite", metadataLegalConstraints["url"])
    if not jsonStringFound:
        findAndReplaceTagString(oafServiceTemplate, "ProviderLicenseWebsite", " ")
        findAndReplaceTagString(oafServiceTemplate, "ProviderLicenseName", " ")
        findAndReplaceTagString(oafServiceTemplate, "DatasetLicenseWebsite", " ")
        findAndReplaceTagString(oafServiceTemplate, "DatasetLicenseName", " ")
        #print(f + ",Warnung,Kein JSON in resourceConstraints")

def get_uuid(datasetMetadataXML):
    """Finds the uuid in the dataset metadata xml and returns it"""
    fileIdentifier = datasetMetadataXML.find(".//gmd:fileIdentifier", namespaces)
    if fileIdentifier is not None:
        uuid = fileIdentifier.find(".//gco:CharacterString", namespaces)
        return uuid.text
    return None

def _process_ServiceProviderData(serviceProvider, metadataTemplateTree, searchString):
    """Helper function to find a specific xml tag in the source xml to save it in the OAF template"""
    templateElem = metadataTemplateTree.find('.//*[.="'+searchString+'"]')
    serviceProviderData = serviceProvider.find(".//ows:" + searchString, namespaces)
    if serviceProviderData is None:
        serviceProviderData = serviceProvider.find(".//ows1.1:" + searchString, namespaces)
    if templateElem.text and serviceProviderData is not None:
        if serviceProviderData.text is not None:
            if not serviceProviderData.text.isspace():
                templateElem.text = serviceProviderData.text
                return
    templateElem.text = " "

def _process_ServiceDatasetCreator(oafServiceTemplate, contactData, templateSearchString, searchString):
    """Helper function to find a specific xml tag in the source xml to save it in the OAF template"""
    templateElem = oafServiceTemplate.find('.//*[.="'+templateSearchString+'"]')
    if contactData is not None:
        datasetCreatorData = contactData.find(".//gmd:" + searchString, namespaces)
        if datasetCreatorData is not None:
            datasetCreatorString = datasetCreatorData.find(".//gco:CharacterString", namespaces)
            if templateElem.text and datasetCreatorString is not None:
                if datasetCreatorString.text is not None:
                    if not datasetCreatorString.text.isspace():
                        templateElem.text = datasetCreatorString.text
                        return
    templateElem.text = " "

def process_ServiceDatasetCreator(datasetMetadataXML, oafServiceTemplate):
    responsibleParties = datasetMetadataXML.findall(".//gmd:CI_ResponsibleParty", namespaces)
    contactData = None
    for responsibleParty in responsibleParties:
        roles = responsibleParty.findall(".//gmd:role", namespaces)
        for role in roles:
            publisher = role.find(".//gmd:CI_RoleCode[@codeListValue='publisher']", namespaces)
            if publisher is not None:
                contactData = responsibleParty
    _process_ServiceDatasetCreator(oafServiceTemplate, contactData, "DatasetCreatorName", "organisationName")
    _process_ServiceDatasetCreator(oafServiceTemplate, contactData, "DatasetCreatorEMail", "electronicMailAddress")
    datasetCreatorUrl = oafServiceTemplate.find('.//*[.="DatasetCreatorUrl"]')
    if contactData is not None:
        datasetCreatorUrlString = contactData.find(".//gmd:URL", namespaces)
        if datasetCreatorUrl.text and datasetCreatorUrlString is not None:
            if datasetCreatorUrlString.text is not None:
                datasetCreatorUrl.text = datasetCreatorUrlString.text
                return
    if datasetCreatorUrl is not None:
        datasetCreatorUrl.text = " "

def process_DatasetServiceIdentification(datasetMetadataXML, metadataTemplateTree, f):
    dataIdentification = datasetMetadataXML.find(".//gmd:MD_DataIdentification", namespaces)
    if dataIdentification is not None:
        serviceIdentificationTitle = metadataTemplateTree.find('.//*[.="ServiceIdentificationTitle"]')
        title = dataIdentification.find(".//gmd:title", namespaces)
        titleString = title.find(".//gco:CharacterString", namespaces)
        if serviceIdentificationTitle.text and titleString.text:
            serviceIdentificationTitle.text = serviceIdentificationTitle.text.replace("ServiceIdentificationTitle", titleString.text)
        else:
            serviceIdentificationTitle.text = " "
        serviceIdentificationAbstract = metadataTemplateTree.find('.//*[.="ServiceIdentificationAbstract"]')
        abstract = dataIdentification.find(".//gmd:abstract", namespaces)
        abstractString = abstract.find(".//gco:CharacterString", namespaces)
        if serviceIdentificationAbstract.text and abstractString.text:
            serviceIdentificationAbstract.text = etree.CDATA(abstractString.text)
        else:
            serviceIdentificationAbstract.text = " "
        return serviceIdentificationTitle.text
    else:
        print(f + ",Fehler,Kein Service Title in MD_DataIdentification")



def process_ServiceProvider(serviceCapabilities, metadataTemplateTree):
    serviceProvider = serviceCapabilities.find(".//ows:ServiceProvider", namespaces)
    if serviceProvider is None:
        serviceProvider = serviceCapabilities.find(".//ows1.1:ServiceProvider", namespaces)
    if serviceProvider is not None:
        _process_ServiceProviderData(serviceProvider, metadataTemplateTree, "ProviderName")
        _process_ServiceProviderData(serviceProvider, metadataTemplateTree, "ProviderSite")
        _process_ServiceProviderData(serviceProvider, metadataTemplateTree, "IndividualName")
        _process_ServiceProviderData(serviceProvider, metadataTemplateTree, "PositionName")
        _process_ServiceProviderData(serviceProvider, metadataTemplateTree, "Phone")
        _process_ServiceProviderData(serviceProvider, metadataTemplateTree, "Facsimile")
        _process_ServiceProviderData(serviceProvider, metadataTemplateTree, "ElectronicMailAddress")
        _process_ServiceProviderData(serviceProvider, metadataTemplateTree, "DeliveryPoint")
        _process_ServiceProviderData(serviceProvider, metadataTemplateTree, "City")
        _process_ServiceProviderData(serviceProvider, metadataTemplateTree, "AdministrativeArea")
        _process_ServiceProviderData(serviceProvider, metadataTemplateTree, "PostalCode")
        _process_ServiceProviderData(serviceProvider, metadataTemplateTree, "Country")
        _process_ServiceProviderData(serviceProvider, metadataTemplateTree, "HoursOfService")
        _process_ServiceProviderData(serviceProvider, metadataTemplateTree, "ContactInstructions")
        _process_ServiceProviderData(serviceProvider, metadataTemplateTree, "Role")
    templateElem = metadataTemplateTree.find('.//*[.="OnlineResource"]')
    serviceProviderData = serviceProvider.find(".//ows:OnlineResource", namespaces)
    if serviceProviderData is None:
        serviceProviderData = serviceProvider.find(".//ows1.1:OnlineResource", namespaces)
    onlineResource = serviceProviderData.get("{http://www.w3.org/1999/xlink}href")
    if templateElem.text and onlineResource is not None:
        templateElem.text = onlineResource
    else:
        templateElem.text = " "

def save_XML_Files(newServiceFilename,metadataTemplateTree, newMetadataFilename, serviceTemplateTree, serviceOnlyIntranet, serviceOnlyInternet):
    
    if serviceOnlyIntranet and not serviceOnlyInternet:
        with open(os.path.join(intranetServicesEndFolder, newServiceFilename), 'wb') as file:
            replaceMetaverURLWithHMDK(serviceTemplateTree)
            file.write(etree.tostring(serviceTemplateTree, pretty_print=True, encoding='utf8', method='xml'))
        with open(os.path.join(intranetMetadataEndFolder, newMetadataFilename), 'wb') as file:
            file.write(etree.tostring(metadataTemplateTree, pretty_print=True, encoding='utf8', method='xml'))
        return
    
    with open(os.path.join(internetServicesEndFolder, newServiceFilename), 'wb') as file:
        file.write(etree.tostring(serviceTemplateTree, pretty_print=True, encoding='utf8', method='xml'))
    with open(os.path.join(internetMetadataEndFolder, newMetadataFilename), 'wb') as file:
        file.write(etree.tostring(metadataTemplateTree, pretty_print=True, encoding='utf8', method='xml'))
    with open(os.path.join(intranetServicesEndFolder, newServiceFilename), 'wb') as file:
        replaceMetaverURLWithHMDK(serviceTemplateTree)
        file.write(etree.tostring(serviceTemplateTree, pretty_print=True, encoding='utf8', method='xml'))
    with open(os.path.join(intranetMetadataEndFolder, newMetadataFilename), 'wb') as file:
        file.write(etree.tostring(metadataTemplateTree, pretty_print=True, encoding='utf8', method='xml'))

#TODO:
def tryHDMK():
    return
    